using UnityEngine;

[CreateAssetMenu(fileName = "Sound", menuName = "ScriptableObjects/Sound", order = 1)]
public class SoundScriptableObject : ScriptableObject
{
    public string SoundName;
    public AudioClip Sound;
}