using UnityEngine;

[CreateAssetMenu(fileName = "SoundsCollection", menuName = "ScriptableObjects/SoundsCollection", order = 1)]
public class SoundsCollection : ScriptableObject
{
    public SoundScriptableObject[] sounds;
}