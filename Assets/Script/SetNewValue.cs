using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AmongUs
{
    public class SetNewValue : MonoBehaviour
    {
        [SerializeField] RoomGeneration myRoom1;
        [SerializeField] RoomGeneration myRoom2;
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                StartCoroutine(myRoom1.RandomizeRoom());
                StartCoroutine(myRoom2.RandomizeRoom());
            }
        }
    }
}

