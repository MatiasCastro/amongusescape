using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AmongUs
{
    [RequireComponent(typeof(LineRenderer))]
    public class Player : Singleton<Player>
    {
        [Range(0, 50)]
        public int segments = 50;
        [Range(0, 5)]
        public float xradius = 1;
        [Range(0, 5)]
        public float yradius = 1;
        LineRenderer line;
        [SerializeField][Range(0, 20)] float speed = 4;
        [SerializeField][Range(0, 20)] float verticalSpeed = 4;
        public List<GameObject> charactersInGame = new List<GameObject>();
        [SerializeField][Range(0f, 1f)] float distnacePerChar = 0.1f;
        public GameObject[] Characters;
        [SerializeField] GameObject corpse;
        [SerializeField][Range(0f, 2f)] float delayToSpawn;
        public int characterAverage;
        bool lose=false;
        public UnityEvent LoseEvent;
        


        void Start()
        {
            line = gameObject.GetComponent<LineRenderer>();
            GameObject go = Instantiate(Characters[0], transform.position, Quaternion.identity);
            charactersInGame.Add(go);
            go = Instantiate(Characters[0], transform.position, Quaternion.identity);
            charactersInGame.Add(go);
            line.positionCount = (segments + 1);
            line.useWorldSpace = false;
            characterAverage = charactersInGame.Count / 2;
            CreatePoints();
        }
        
        private void FixedUpdate()
        {

            if (charactersInGame.Count<1)
            {
                if (lose==false)
                {
                    lose=true;
                    LoseEvent.Invoke();

                }
            }
            Vector3 direction = new Vector3(speed, 0, 0);
            Vector3 verticalDirection = new Vector3(0, 0, verticalSpeed);
            transform.position += (verticalDirection * Time.deltaTime);
            if (Input.GetKey("a"))
            {
                transform.position += -(direction * Time.deltaTime);
            }
            if (Input.GetKey("d"))
            {
                transform.position += (direction * Time.deltaTime);
            }
        }
        public GameObject AddUnits(Vector3 position)
        {
            GameObject unit = Instantiate(Characters[0], position, Quaternion.identity);
            charactersInGame.Add(unit);
            xradius += distnacePerChar;
            yradius += distnacePerChar;
            CreatePoints();
            return unit;
        }
        public void DestroyUnits()
        {
            GameObject toKill = charactersInGame[charactersInGame.Count - 1];
            Vector3 position = toKill.transform.position;
            Quaternion rotation = toKill.transform.rotation;
            xradius -= distnacePerChar;
            yradius -= distnacePerChar;
            StartCoroutine(SpawnCorpse(position, rotation));
            charactersInGame.Remove(toKill);
            Destroy(toKill);
            
        }
        public void DestroySpecificUnit(GameObject unit)
        {
            GameObject toKill = unit;
            Vector3 position = toKill.transform.position;
            Quaternion rotation = toKill.transform.rotation;
            xradius -= distnacePerChar;
            yradius -= distnacePerChar;
            StartCoroutine(SpawnCorpse(position, rotation));
            charactersInGame.Remove(toKill);
            Destroy(toKill);
            SoundController.GetInstance().NewSound("Kill");
        }
        IEnumerator SpawnCorpse(Vector3 position, Quaternion rotation)
        {
            yield return new WaitForSeconds(delayToSpawn);
            Instantiate(corpse, position, rotation);
        }

        void CreatePoints()
        {
            float x;
            float y;
            float z;

            float angle = 20f;
            line.endWidth = 0.1f;
            line.startWidth = 0.1f;
            for (int i = 0; i < (segments + 1); i++)
            {
                x = Mathf.Sin(Mathf.Deg2Rad * angle) * xradius;
                y = Mathf.Cos(Mathf.Deg2Rad * angle) * yradius;

                line.SetPosition(i, new Vector3(x, y, 0));

                angle += (360f / segments);
            }
        }
    }
}