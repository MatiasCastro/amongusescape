using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmongUs
{
    public class Movement : MonoBehaviour
    {
        [SerializeField] Vector3 differencePosition;
        [SerializeField][Range(0, 20)] float speed = 4;

        private Rigidbody rigidBody;
        private void Start()
        {
            rigidBody = GetComponent<Rigidbody>();

            newPosition();
        }
        private void newPosition()
        {
            float x = Random.Range(-Player.GetInstance().xradius, Player.GetInstance().xradius);
            float y = Random.Range(-Player.GetInstance().yradius, Player.GetInstance().yradius);
            differencePosition = new Vector3(x, 0, y);
        }
        private void FixedUpdate()
        {
            transform.position = Vector3.MoveTowards(transform.position, Player.GetInstance().transform.position + differencePosition, speed * Time.deltaTime);
            Vector3 lookAtPos = Player.GetInstance().transform.position + differencePosition;
            lookAtPos.y = transform.position.y;
            transform.LookAt(lookAtPos);
        }

    }
}