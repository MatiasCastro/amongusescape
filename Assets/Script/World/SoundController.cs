using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AmongUs
{
    public class SoundController : Singleton<SoundController>
    {
        [SerializeField] AudioSource audioSource;
        [SerializeField] AudioSource audioSource2;
        [SerializeField] SoundsCollection soundsCollection;
        // Start is called before the first frame update
        public void NewSound(string SoundName)
        {
            List<AudioClip> posibbleClips = new List<AudioClip>();
            foreach (SoundScriptableObject sound in soundsCollection.sounds)
            {
                if (sound.SoundName == SoundName)
                {
                    posibbleClips.Add(sound.Sound);
                }
            }
            if (posibbleClips.Count > 0)
            {
                if (!audioSource.isPlaying)
                { 
                    audioSource.clip = posibbleClips[Random.Range(0, posibbleClips.Count - 1)];
                    audioSource.Play();
                }
                else
                {
                    audioSource2.clip = posibbleClips[Random.Range(0, posibbleClips.Count - 1)];
                    audioSource2.Play();
                }
            }
        }
    }
}