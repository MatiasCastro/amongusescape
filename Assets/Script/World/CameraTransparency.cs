using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransparency : MonoBehaviour
{
    [SerializeField] GameObject opaqueObject;
    [SerializeField] GameObject transparencyObject;

    private void TransparencyObjects()
    {
        opaqueObject.SetActive(false);
        transparencyObject.SetActive(true);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            TransparencyObjects();
        }
    }
}
