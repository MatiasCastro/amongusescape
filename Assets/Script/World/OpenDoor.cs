using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AmongUs
{
    public class OpenDoor : MonoBehaviour
    {
        [SerializeField] Door myDoor;

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                SoundController.GetInstance().NewSound("DoorOpen");
                myDoor.on = true;
            }
        }
    }
}