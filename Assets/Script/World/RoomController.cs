using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AmongUs
{
    public class RoomController : MonoBehaviour
    {
        [SerializeField] RoomGeneration[] doors;
        public GameObject ThisRoom;
        public UnityEvent MoveThisRoom;

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                MoveThisRoom.Invoke();
            }
        }
    }
}