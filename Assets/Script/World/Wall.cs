using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
namespace AmongUs
{
    public class Wall : MonoBehaviour
    {
        public UnityEvent CollisionDetect;
        bool used = false;
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                CollisionDetect.Invoke();
                used = true;
            }
        }
    }
}