using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AmongUs
{
    public class Door : MonoBehaviour
    {
        public bool on = false;
        [SerializeField][Range(0f, 5f)] float downSpeed;
        public float yBase;
        [SerializeField] RoomGeneration myRoom;
        private void Start()
        {
            yBase = transform.position.y;
            myRoom.ReloadTexts.AddListener(resetDoor);
            
        }

        private void resetDoor(string arg0, Color arg1)
        {
            on=false;
            transform.position=new Vector3(transform.position.x,yBase,transform.position.z);
        }

        void Update()
        {
            if (on == false)
                return;
            transform.position -= new Vector3(0, downSpeed * Time.deltaTime, 0);
        }
    }
}