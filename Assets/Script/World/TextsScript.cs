using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
namespace AmongUs
{
    public class TextsScript : MonoBehaviour
    {
        [SerializeField] RoomGeneration myDoor;

        [SerializeField] TextMeshProUGUI[] ammountText;
        // Start is called before the first frame update
        void Start()
        {
            myDoor.ReloadTexts.AddListener(RefreshText);
        }

        void RefreshText(string text, Color color)
        {
            for (int i = 0; i < ammountText.Length; i++)
            {
                ammountText[i].text = text;
                ammountText[i].color = color;
            }
        }
    }
}