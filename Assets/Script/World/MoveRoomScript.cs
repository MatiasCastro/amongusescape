using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace AmongUs
{
    public class MoveRoomScript : MonoBehaviour
    {
        [SerializeField] GameObject[] rooms=new GameObject[3];
        [SerializeField] float difference;
        private void Start()
        {
            rooms[0].GetComponent<RoomController>().MoveThisRoom.AddListener(moveRoom);
            rooms[1].GetComponent<RoomController>().MoveThisRoom.AddListener(moveRoom);
            rooms[2].GetComponent<RoomController>().MoveThisRoom.AddListener(moveRoom);
        }
        private void moveRoom()
        {
            rooms[0].GetComponent<RoomController>().ThisRoom.transform.position=new Vector3(rooms[2].GetComponent<RoomController>().ThisRoom.transform.position.x,rooms[2].GetComponent<RoomController>().ThisRoom.transform.position.y,rooms[2].GetComponent<RoomController>().ThisRoom.transform.position.z+difference);
            GameObject[] newArray= rooms;
            GameObject toSave=newArray[0];
            newArray[0]=newArray[(newArray.Length)-1];
            for (int i=2;i<rooms.Length;i++)
            {
                print(i);
                newArray[i]=newArray[i-1];
            }
            newArray[1]=toSave;
            /*
            newArray[0]=newArray[2];
            newArray[2]=newArray[1];
            newArray[1]=toSave;*/
            rooms=newArray;
        }
    }
}
