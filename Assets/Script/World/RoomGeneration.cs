using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

namespace AmongUs
{
    public class RoomGeneration : MonoBehaviour
    {
        [SerializeField] Color goodColor;
        [SerializeField] Color badColor;
        [SerializeField] int ammount;
        [SerializeField] Transform spawnPoint;
        [SerializeField] Wall myDoor;
        [SerializeField] bool add;
        [SerializeField] bool substract;
        [SerializeField] bool division;
        [SerializeField] bool multiplication;

        [SerializeField] Animator impostor;
        [SerializeField] Material[] lights;
        [SerializeField] MeshRenderer myMesh;
        public float toAverage=0;
        int PlayerCount => Player.GetInstance().charactersInGame.Count;
        int randomNum;
        public UnityEvent<string, Color> ReloadTexts;
        public UnityEvent Ready;



        void Start()
        {
            //myMesh=gameObject.GetComponent<MeshRenderer>();
            myDoor.CollisionDetect.AddListener(OnRoomEnter);

        }
        public IEnumerator RandomizeRoom()
        {
            add = false;
            substract = false;
            multiplication = false;
            division = false;


            yield return new WaitForSeconds(0.1f);

            if (PlayerCount >= 2)
                randomNum = Random.Range(0, 4);
            else
                randomNum = Random.Range(2, 4);
            int newAmmount;
            switch (randomNum)
            {
                case 0:
                    impostor.gameObject.SetActive(true);
                    newAmmount = Random.Range(1, PlayerCount);
                    ammount = Mathf.Clamp(newAmmount, 1, PlayerCount - 1);
                    toAverage=-ammount;
                    substract = true;
                    break;
                case 1:
                    impostor.gameObject.SetActive(true);
                    newAmmount = Random.Range(1, 3);
                    ammount = newAmmount;
                    division = true;
                    break;

                case 2:
                    impostor.gameObject.SetActive(false);
                    newAmmount = Random.Range(1, 5);
                    ammount = newAmmount;
                    add = true;
                    toAverage=ammount;
                    break;
                case 3:
                    impostor.gameObject.SetActive(false);
                    newAmmount = Random.Range(1, 3);
                    ammount = newAmmount;
                    multiplication = true;
                    break;
            }
            Ready.Invoke();
            RefreshText();
        }

        void OnRoomEnter()
        {

            if (add == true)
            {
                AddCharacters();
                return;
            }
            if (substract == true)
            {
                DieScript.GetInstance().StartDie(this);
                return;
            }
            if (division == true)
            {
                DieScript.GetInstance().StartDie(this);
                return;
            }
            if (multiplication == true)
            {
                MultiplicationCharacters();
                return;
            }

        }
        public void CharacterStandUp()
        {
            impostor.SetBool("Escape", false);
            impostor.SetBool("Up", true);
        }
        public void CharacterHidding()
        {
            impostor.SetBool("Up", false);
            impostor.SetBool("Escape", true);
        }

        void AddCharacters()
        {
            for (int i = 0; i < ammount; i++)
            {
                Player.GetInstance().AddUnits(new Vector3(spawnPoint.position.x, spawnPoint.position.y, spawnPoint.position.z));
            }
        }


        void SubstratCharacters()
        {
            for (int i = 0; i < ammount; i++)
            {
                Player.GetInstance().DestroyUnits();
            }
        }
        public void DieAnimationReturn()
        {
            if (substract == true)
            {
                SubstratCharacters();
                return;
            }
            if (division == true)
            {
                Divisionharacters();
                return;
            }
        }

        void Divisionharacters()
        {
            int cant = (int)(Player.GetInstance().charactersInGame.Count - ((PlayerCount) / ammount));
            toAverage=-cant;
            for (int i = 0; i < cant; i++)
            {
                Player.GetInstance().DestroyUnits();
            }
        }
        void MultiplicationCharacters()
        {
            int cant = (ammount * Player.GetInstance().charactersInGame.Count) - PlayerCount;
            toAverage=cant;
            for (int i = 0; i < cant; i++)
            {
                Player.GetInstance().AddUnits(new Vector3(spawnPoint.position.x, spawnPoint.position.y, spawnPoint.position.z));
            }
        }

        void RefreshText()
        {
            if (add == true)
            {
                ReloadTexts.Invoke("+" + ammount.ToString(), goodColor);
                var mats = myMesh.materials;
                mats[1] = lights[0];
                myMesh.materials = mats;

                return;
            }
            if (substract == true)
            {
                ReloadTexts.Invoke("-" + ammount.ToString(), badColor);
                var mats = myMesh.materials;
                mats[1] = lights[1];
                myMesh.materials = mats;

                return;
            }
            if (division == true)
            {
                ReloadTexts.Invoke("/" + ammount.ToString(), badColor);
                var mats = myMesh.materials;
                mats[1] = lights[1];
                myMesh.materials = mats;

                return;
            }
            if (multiplication == true)
            {
                ReloadTexts.Invoke("x" + ammount.ToString(), goodColor);
                var mats = myMesh.materials;
                mats[1] = lights[0];
                myMesh.materials = mats;

                return;
            }
        }
    }
}