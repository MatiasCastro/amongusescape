using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AmongUs
{
    public class DieScript : Singleton<DieScript>
    {
        public UnityEvent DieEvent;
        [SerializeField] Animator myAnimator;
        RoomGeneration lastCall;

        public void DieCall()
        {
            DieEvent.Invoke();
        } 
        public void StartDie(RoomGeneration call)
        {
            myAnimator.SetBool("Die",true);
            lastCall=call;
        }
        public void Kill()
        {
            lastCall.DieAnimationReturn();
            
            SoundController.GetInstance().NewSound("Kill");
        }
        public void CharacterHidding()
        {
            lastCall.CharacterHidding();
        }
        public void CharacterStandUp()
        {
            lastCall.CharacterStandUp();
        }
        public void StopDie()
        {
            SoundController.GetInstance().NewSound("Trapdoor");
            myAnimator.SetBool("Die",false);
        }
        
        public void DestrySelf()
        {
            Destroy(gameObject);
        }
    }
}