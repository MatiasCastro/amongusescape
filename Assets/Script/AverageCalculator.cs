using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AmongUs
{
    public class AverageCalculator : MonoBehaviour
    {
        [SerializeField] RoomGeneration room1;
        [SerializeField] RoomGeneration room2;
        bool room1State = false;
        bool room2State = false;


        private void Awake()
        {
            room1.Ready.AddListener(Room1Check);
            room2.Ready.AddListener(Room2Check);
        }
        void Room1Check()
        {
            room1State = true;
        }
        void Room2Check()
        {
            room2State = true;
        }
        private void Update()
        {
            if (room1State == true && room2State == true)
            {
                room1State = false;
                room2State = false;
                StartCoroutine(RefreshText());
            }
        }
        IEnumerator RefreshText()
        {
            yield return new WaitForSeconds(1);

            if ((room1.toAverage) < (room2.toAverage))
            {
                Player.GetInstance().characterAverage += (int)(room2.toAverage/ 1.5f);
            }

            else
                Player.GetInstance().characterAverage += (int)(room1.toAverage / 1.5f);
        }
    }
}
