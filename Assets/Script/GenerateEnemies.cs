using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AmongUs
{
    public class GenerateEnemies : MonoBehaviour
    {
        [SerializeField] GameObject enemy;
        [SerializeField] Transform spawnpoint;
        void enemiesGeneration()
        {
            int amount=Mathf.Clamp(Player.GetInstance().characterAverage,1,20);
            Debug.Log("se generaron: "+amount);
            List<GameObject> possibleVictims=new (Player.GetInstance().charactersInGame);

            for(int i=0;i<amount;i++)
            {
                Debug.Log("entro al for");
                GameObject go = Instantiate(enemy,spawnpoint);
                int randomNum=Random.Range(0,possibleVictims.Count-1);
                go.GetComponent<EnemyController>().objetive=possibleVictims[randomNum];
                possibleVictims.Remove(possibleVictims[randomNum]);
            }
            Debug.Log("entro al for");
            Player.GetInstance().characterAverage=0;
        }
        private void OnTriggerEnter(Collider other)
        {
            if(other.tag=="Player")
            {
                enemiesGeneration();
            }
        }
    }
}