using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AmongUs
{
    public class EnemyController : MonoBehaviour
    {
        public GameObject objetive;
        Animator animator;
        [SerializeField] float speed;
        private void Start()
        {
            animator = GetComponent<Animator>();
        }
        void Update()
        {
            if (objetive == null)
                return;
            transform.position = Vector3.MoveTowards(transform.position, objetive.transform.position, speed * Time.deltaTime);
            transform.LookAt(objetive.transform);
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == objetive)
            {
                animator.SetBool("Kill", true);
                Player.GetInstance().DestroySpecificUnit(objetive);
            }

        }
    }
}
